<?php

namespace Uts\HotelBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class HotelRepository
 * @package Uts\HotelBundle\Manager
 */
class HotelRepository extends EntityRepository
{
    /**
     * @param $requestId
     * @return QueryBuilder
     */
    public function getHotelListQueryBuilderByRequestId($requestId)
    {
        $qb = $this->createQueryBuilder('hotel');

        $qb
            ->addSelect('searchResults')
            ->addSelect('request')
            ->addSelect('meal')
            ->leftJoin('hotel.searchResults', 'searchResults')
            ->leftJoin('searchResults.meal', 'meal')
            ->leftJoin('searchResults.request', 'request')
            ->where('searchResults.request = :requestId')
            ->setParameter('requestId', $requestId)
            ;

        return $qb;
    }
}
