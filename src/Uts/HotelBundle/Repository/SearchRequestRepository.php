<?php

namespace Uts\HotelBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Uts\HotelBundle\Entity\SearchRequest;

/**
 * Class SearchRequestRepository
 * @package Uts\HotelBundle\Manager
 */
class SearchRequestRepository extends EntityRepository
{
    /**
     * @param SearchRequest $searchRequest
     * @return SearchRequest|null
     */
    public function findSameSearchRequest(SearchRequest $searchRequest)
    {
        $qb = $this->createQueryBuilder('searchRequest');

        $qb
            ->where('searchRequest.adults = :adults')
            ->andWhere('searchRequest.city = :city')
            ->andWhere('searchRequest.checkIn = :checkIn')
            ->andWhere('searchRequest.checkOut = :checkOut')
            ->andWhere('searchRequest.status = :status')
            ->setParameter('adults', $searchRequest->getAdults())
            ->setParameter('city', $searchRequest->getCity(), Type::OBJECT)
            ->setParameter('checkIn', $searchRequest->getCheckIn(), Type::DATE)
            ->setParameter('checkOut', $searchRequest->getCheckOut(), Type::DATE)
            ->setParameter('status', SearchRequest::STATUS_COMPLETE)
            ->orderBy('searchRequest.createdAt', 'DESC')
            ->setMaxResults(1)
            ;

        return $qb->getQuery()->getOneOrNullResult();
    }
}
