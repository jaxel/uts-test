<?php

namespace Uts\HotelBundle\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Uts\HotelBundle\Entity\City;
use Uts\HotelBundle\Entity\Country;
use Uts\HotelBundle\Entity\Hotel;
use Uts\HotelBundle\Entity\SpecialOffer;

/**
 * Class SpecialOfferRepository
 * @package Uts\HotelBundle\Manager
 */
class SpecialOfferRepository extends EntityRepository
{
    /**
     * @param Hotel $hotel
     * @return SpecialOffer[]
     */
    public function getBestSpecialOfferListByHotel(Hotel $hotel)
    {
        $qb = $this->createQueryBuilder('specialOffer');

        $qb
            ->where('specialOffer.isActive = :active')
            ->andWhere('specialOffer.hotel = :hotel')
            ->setParameter('hotel', $hotel, Type::OBJECT)
            ->setParameter('active', true, Type::BOOLEAN)
            ->orderBy('specialOffer.discountValue', 'DESC')
            ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param City $city
     * @return SpecialOffer[]
     */
    public function getBestSpecialOfferListByCity(City $city)
    {
        $qb = $this->createQueryBuilder('specialOffer');

        $qb
            ->where('specialOffer.isActive = :active')
            ->andWhere('specialOffer.city = :city')
            ->andWhere('specialOffer.hotel is null')
            ->setParameter('city', $city, Type::OBJECT)
            ->setParameter('active', true, Type::BOOLEAN)
            ->orderBy('specialOffer.discountValue', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Country $country
     * @return SpecialOffer[]
     */
    public function getBestSpecialOfferListByCountry(Country $country)
    {
        $qb = $this->createQueryBuilder('specialOffer');

        $qb
            ->where('specialOffer.isActive = :active')
            ->andWhere('specialOffer.country = :country')
            ->andWhere('specialOffer.hotel is null')
            ->andWhere('specialOffer.city is null')
            ->setParameter('country', $country, Type::OBJECT)
            ->setParameter('active', true, Type::BOOLEAN)
            ->orderBy('specialOffer.discountValue', 'DESC')
        ;

        return $qb->getQuery()->getResult();
    }
}
