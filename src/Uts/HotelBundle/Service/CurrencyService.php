<?php

namespace Uts\HotelBundle\Service;
use Doctrine\ORM\EntityManager;
use Uts\HotelBundle\Entity\Currency;

/**
 * Class CurrencyService
 * @package Uts\HotelBundle\Service
 */
class CurrencyService
{
    const BASE_CURRENCY = 'RUB';

    /** @var EntityManager */
    protected $em;

    /** @var array */
    protected $currencyList;

    /**
     * CurrencyService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    protected function getCurrencyList()
    {
        if(!$this->currencyList) {
            $currencyList = $this->em->getRepository('UtsHotelBundle:Currency')->findAll();

            /** @var Currency $currency */
            foreach ($currencyList as $currency) {
                $this->currencyList[$currency->getCode()] = $currency->getRate();
            }
        }

        return $this->currencyList;
    }

    /**
     * @param $value
     * @param $fromCurrencyCode
     * @param $toCurrencyCode
     * @return float
     * @throws \Exception
     */
    public function convert($value, $fromCurrencyCode, $toCurrencyCode)
    {
        $currencyCodeList = array_keys($this->getCurrencyList());

        if(!in_array($fromCurrencyCode,  $currencyCodeList) || (!in_array($fromCurrencyCode,  $currencyCodeList))) {
            throw new \Exception('Unknown currency code');
        }

        $fromToBaseCurrencyRate = $this->getCurrencyList()[$fromCurrencyCode];

        $toToBaseCurrencyRate = $this->getCurrencyList()[$toCurrencyCode];

        if($toCurrencyCode = self::BASE_CURRENCY) {
            $toToBaseCurrencyRate = 1/$toToBaseCurrencyRate;
        }

        return round($value * $fromToBaseCurrencyRate * $toToBaseCurrencyRate, 2);
    }
}