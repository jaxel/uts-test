<?php

namespace Uts\HotelBundle\Service;

use Doctrine\ORM\EntityManager;
use Uts\HotelBundle\Entity\SearchResult;
use Uts\HotelBundle\Entity\SpecialOffer;

/**
 * Class SpecialOfferService
 * @package Uts\HotelBundle\Service
 */
class SpecialOfferService
{
    /** @var EntityManager */
    protected $em;

    /** @var CurrencyService */
    protected $currencyService;

    /**
     * SpecialOfferService constructor.
     * @param EntityManager $em
     * @param CurrencyService $currencyService
     */
    public function __construct(EntityManager $em, CurrencyService $currencyService)
    {
        $this->em = $em;
        $this->currencyService = $currencyService;
    }


    /**
     * @param SearchResult $searchResult
     * @return SpecialOffer|null
     */
    public function getSpecialOfferForSearchResult(SearchResult $searchResult)
    {
        $specialOfferRepository = $this->em->getRepository('UtsHotelBundle:SpecialOffer');

        $specialOfferList = $specialOfferRepository->getBestSpecialOfferListByHotel($searchResult->getHotel());

        if ($specialOfferList) {
            return $this->chooseBestSpecialOfferFromSpecialOfferList($specialOfferList, $searchResult);
        }

        $specialOfferList = $specialOfferRepository->getBestSpecialOfferListByCity(
            $searchResult->getHotel()->getCity()
        );

        if ($specialOfferList) {
            return $this->chooseBestSpecialOfferFromSpecialOfferList($specialOfferList, $searchResult);
        }

        $specialOfferList = $specialOfferRepository->getBestSpecialOfferListByCountry(
            $searchResult->getHotel()->getCity()->getCountry()
        );

        if ($specialOfferList) {
            return $this->chooseBestSpecialOfferFromSpecialOfferList($specialOfferList, $searchResult);
        }

        return null;
    }

    /**
     * @param array $specialOfferList
     * @param SearchResult $searchResult
     * @return null|SpecialOffer
     */
    protected function chooseBestSpecialOfferFromSpecialOfferList(array $specialOfferList, SearchResult $searchResult)
    {
        /** @var SpecialOffer|null $bestSpecialOffer */
        $bestSpecialOffer = null;

        foreach ($specialOfferList as $specialOffer) {
            if (!$bestSpecialOffer) {
                $bestSpecialOffer = $specialOffer;
            } else {
                $bestSpecialOfferDiscount = $this->calculateDiscount(
                    $searchResult->getPrice(),
                    $searchResult->getCurrency(),
                    $bestSpecialOffer
                );

                $specialOfferDiscount = $this->calculateDiscount(
                    $searchResult->getPrice(),
                    $searchResult->getCurrency(),
                    $specialOffer
                );

                if ($specialOfferDiscount > $bestSpecialOfferDiscount) {
                    $bestSpecialOffer = $specialOffer;
                }
            }
        }

        return $bestSpecialOffer;
    }

    /**
     * @param $price
     * @param $currencyCode
     * @param SpecialOffer $specialOffer
     * @return float|int
     */
    public function calculateDiscount($price, $currencyCode, SpecialOffer $specialOffer)
    {
        if ($specialOffer->getDiscountType() == SpecialOffer::DISCOUNT_TYPE_ABSOLUTE) {
            return $this->currencyService->convert(
                $specialOffer->getDiscountValue(),
                'RUB',
                $currencyCode
            );
        }

        if ($specialOffer->getDiscountType() == SpecialOffer::DISCOUNT_TYPE_MULTIPLIER) {
            return $specialOffer->getDiscountValue() / 100 * $price;
        }

        return 0;
    }
}